# Defined in - @ line 1
function ace --wraps='emerge --update --deep --with-bdeps=y --newuse @world' --wraps='doas emerge --update --deep --with-bdeps=y --newuse @world' --description 'alias ace doas emerge --update --deep --with-bdeps=y --newuse @world'
  doas emerge --update --deep --with-bdeps=y --newuse @world $argv;
end
